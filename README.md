# meta-linux4space

## General description

The meta-linux4space is part of the Linux4Space project. 

The layer is dedicated to the distro configuration and the OS related configuration:

* The main distro configuration
* Image definitions
* Kernel related recipes
* Bootloader related recipes
* All the kernel-space applications
* System configuration
# Copyright (C) 2021 Enrico Jorns <ejo@pengutronix.de>
# Released under the MIT license (see COPYING.MIT for the terms)

#DESCRIPTION = "Boot images"
#LICENSE = "MIT"

inherit nopackages deploy

do_fetch[noexec] = "1"
do_patch[noexec] = "1"
do_compile[noexec] = "1"
do_install[noexec] = "1"
#deltask do_populate_sysroot
do_install() {
}
do_deploy () {
}
addtask deploy after do_install

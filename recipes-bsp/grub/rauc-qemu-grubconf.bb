SUMMARY = "Grub configuration file to use with RAUC"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

include conf/image-uefi.conf

RPROVIDES:${PN} += "virtual-grub-bootconf"

SRC_URI += " \
    file://grub.cfg \
    file://grubenv \
    file://fw_env.config \
    "

S = "${WORKDIR}"

inherit deploy

do_install() {
        install -d ${D}${EFI_FILES_PATH}
        install -d ${D}${sysconfdir}
        install -m 644 ${WORKDIR}/grub.cfg ${D}${EFI_FILES_PATH}/grub.cfg
        install -m 644 ${WORKDIR}/fw_env.config ${D}${sysconfdir}/fw_env.config
        #install -m 644 ${WORKDIR}/fw_env.config ${D}${sysconfdir}/fw_env.config1
}

FILES:${PN} += "${EFI_FILES_PATH} ${sysconfig}/fw_env.config "

do_deploy() {
	install -m 644 ${WORKDIR}/grub.cfg ${DEPLOYDIR}
	install -m 644 ${WORKDIR}/grubenv ${DEPLOYDIR}
    #install -m 644 ${WORKDIR}/fw_env.config ${DEPLOYDIR}
}

addtask deploy after do_install before do_build

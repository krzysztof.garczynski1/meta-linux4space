FILESEXTRAPATHS:prepend := "${THISDIR}/rauc:"
SRC_URI:append := " file://system.conf.${MACHINE}"
SRC_URI:append := " file://fw_env.config"
do_install:append(){
            install -m 0644 ${WORKDIR}/system.conf.${MACHINE} ${D}${sysconfdir}/rauc/system.conf
# maybe not required or different on other machines than raspberry
            install -m 0644 ${WORKDIR}/fw_env.config  ${D}${sysconfdir}/fw_env.config
}
do_install:append:raspberrypi3(){
            install -m 0644 ${WORKDIR}/fw_env.config  ${D}${sysconfdir}/fw_env.config
}
do_install:append:raspberrypi4(){
            install -m 0644 ${WORKDIR}/fw_env.config  ${D}${sysconfdir}/fw_env.config
}
RDEPENDS:${PN}:raspberrypi3 += "e2fsprogs-mke2fs"
RDEPENDS:${PN}:raspberrypi4 += "e2fsprogs-mke2fs"
RDEPENDS:${PN}:qemux86-64 += "grub-editenv e2fsprogs-mke2fs"

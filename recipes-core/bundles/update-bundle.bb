DESCRIPTION = "RAUC bundle generator"

inherit bundle
include ${MACHINE}-update-bundle.inc

RAUC_KEY_FILE = "${THISDIR}/files/development-1.key.pem"
RAUC_CERT_FILE = "${THISDIR}/files/development-1.cert.pem"
